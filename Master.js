'use strict'
//Hello WOrld!
console.log('Hello World!');
let x =  'Welcome to 10 Days of JavaScript!'
console.log(x);
//Data types
let firstInteger = '4',
    firstDecimal = '4.0',
    firstString = 'HackerRank';
let secondInteger = '12',
    secondDecimal = '4.32',
    secondString = 'is the best place to learn and practice coding!.';
console.log(parseInt(firstInteger) + parseInt(secondInteger));
console.log(parseFloat(firstDecimal) + parseFloat(secondDecimal));
console.log(firstString + ' ' + secondString);
//Arithmetic Operator
let length = 3,
    width = 4.5;
const area = length * width,
    parameter = 2 * (length + width);
console.log(area);
console.log(parameter);
//Functions
function factorial(n) {
    return (n != 1) ? n * factorial(n - 1) : 1;
}
console.log(factorial(4));
//Conditional Statments: If-Else
function getGrade(score) {
    if (score > 25 && score <= 30) {
      return "A";
    } else if (score > 20 && score <= 25) {
      return "B";
    } else if (score > 15 && score <= 20) {
      return "C";
    } else if (score > 10 && score <= 15) {
      return "D";
    } else if (score > 5 && score <= 10) {
      return "E";
    } else if (score > 0 && score <= 5) {
      return "F";
    } else {
      return console.log("Fallen");
    }
  }
console.log(getGrade(11));  